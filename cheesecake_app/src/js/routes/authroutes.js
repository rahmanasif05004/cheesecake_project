import LoginPage from '../../pages/auth/login';
import HomePage from '../../pages/home';
import RegistrationPage from '../../pages/auth/registration';
import UserprofilePage from '../../pages/auth/user-profile';
import ProfileupdatePage from '../../pages/auth/user-update.vue';
import store from '../stores';
import IntroPage from '../../pages/intro.vue';
const AuthRoutes = [{
        path: '/',
        async (routeTo, routeFrom, resolve, reject) {
            if(localStorage.getItem('app-initialized')===null)
            {
                resolve({
                    component:IntroPage,
                });
            }
            else
            {
                resolve({
                    component: store.getters.isLoggedIn ? HomePage : LoginPage,
                });
            }
        }
    },
    {
        path: '/login',
        component: LoginPage,
    },
    {
        path: '/registration',
        component: RegistrationPage,
    },
    {
        path: '/userprofile/',
        component: UserprofilePage,
    },
    {
        path: '/profileupdate/',
        component: ProfileupdatePage,
    },

]

export default AuthRoutes;