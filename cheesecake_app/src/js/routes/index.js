import HomePage from '../../pages/home.vue';
import AboutPage from '../../pages/about.vue';
import ContactPage from '../../pages/contact-us.vue';
import FoodtruckPage from '../../pages/food-truck.vue';
import GalleryPage from '../../pages/gallery.vue';
import PrivacyPage from '../../pages/policy.vue';
import CartviewPage from '../../pages/cart-view.vue';
import CheckoutPage from '../../pages/checkout.vue';
import ConditionsPage from '../../pages/trams-conditions.vue';
import IntroPage from '../../pages/intro.vue';
import ForgotPage from '../../pages/forgot.vue';
import ProductdetailsGallery from '../../pages/product-details-gallery.vue';

import DynamicRoutePage from '../../pages/dynamic-route.vue';
import RequestAndLoad from '../../pages/request-and-load.vue';
import NotFoundPage from '../../pages/404.vue';
import NoInternetPage from '../../pages/no-internet';

//authentication routes
import AuthRoutes from '../routes/authroutes';
import ProductRoutes from '../routes/productroutes';
import OrderRoutes from '../routes/orderroutes';

var routes = [
  ...AuthRoutes,
  ...ProductRoutes,
  ...OrderRoutes,
  {
    path:'/no-internet/',
    component: NoInternetPage,
  },
  {
    path: '/home',
    component: HomePage,
  },
  {
    path: '/about/',
    component: AboutPage,
  },
  {
    path: '/contact-us/',
    component: ContactPage,
  },
  {
    path: '/food-truck/',
    component: FoodtruckPage,
  },
  {
    path: '/gallery/',
    component: GalleryPage,
  },
  {
    path: '/intro/',
    component: IntroPage,
  },
  {
    path: '/privacy-policy/',
    component: PrivacyPage,
  },
  {
    path: '/cart-view/',
    component: CartviewPage,
  },
  {
    path: '/checkout/',
    component: CheckoutPage,
  },
  {
    path: '/trams-conditions/',
    component: ConditionsPage,
  },
  {
    path: '/forgot/',
    component: ForgotPage,
  },
  {
    path: '/productdetailsgallery/',
    component: ProductdetailsGallery,
    options: {
      transition: "f7-circle", // Animation Link : https://framework7.io/docs/view.html#custom-page-transitions
    },
  },


  {
    path: '/dynamic-route/blog/:blogId/post/:postId/',
    component: DynamicRoutePage,
  },
  {
    path: '/request-and-load/user/:userId/',
    async: function (routeTo, routeFrom, resolve, reject) {
      // Router instance
      var router = this;

      // App instance
      var app = router.app;

      // Show Preloader
      app.preloader.show();

      // User ID from request
      var userId = routeTo.params.userId;

      // Simulate Ajax Request
      setTimeout(function () {
        // We got user data from request
        var user = {
          firstName: 'Vladimir',
          lastName: 'Kharlampidi',
          about: 'Hello, i am creator of Framework7! Hope you like it!',
          links: [{
              title: 'Framework7 Website',
              url: 'http://framework7.io',
            },
            {
              title: 'Framework7 Forum',
              url: 'http://forum.framework7.io',
            },
          ]
        };
        // Hide Preloader
        app.preloader.hide();

        // Resolve route to load page
        resolve({
          component: RequestAndLoad,
        }, {
          context: {
            user: user,
          }
        });
      }, 1000);
    },
  },
  {
    path: '(.*)',
    component: NotFoundPage,
  },
];

export default routes;