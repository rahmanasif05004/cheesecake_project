import ProductlistPage from '../../pages/products/product-list.vue';
import ProductDetailsPage from '../../pages/products/product-details.vue';
const ProductRoutes = [{
        path: '/product-list/',
        component: ProductlistPage,
    },
    {
        path: '/product-details/:productId',
        component: ProductDetailsPage,
    }
];

export default ProductRoutes;