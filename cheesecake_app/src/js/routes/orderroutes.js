import OrderlistPage from '../../pages/orders/order-list.vue';
import OrderdetailsPage from '../../pages/orders/order-details';
const OrderRoutes=[
    {
        path: '/order-list/',
        component: OrderlistPage,
      },
      {
        path: '/order-details/:orderId',
        component: OrderdetailsPage,
      },
];

export default OrderRoutes;