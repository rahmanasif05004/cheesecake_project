// Import Vue
import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import AppConfig from '../js/config/appConfig';
Vue.use(VueAxios, axios);


// Import Framework7
import Framework7 from 'framework7/framework7-lite.esm.bundle.js';

// Import Framework7-Vue Plugin
import Framework7Vue from 'framework7-vue/framework7-vue.esm.bundle.js';

// Import Framework7 Styles
import 'framework7/css/framework7.bundle.css';

// Import Icons and App Custom Styles
import '../css/icons.css';
import '../css/app.css';
import '../css/custom.css';
import '../css/style.css';

// Import App Component
import App from '../components/app.vue';
import store from '../js/stores';


//set axios base url
axios.defaults.baseURL = AppConfig.apiUrl;
if (store.getters.accessToken) {
  axios.defaults.headers.common["Authorization"] = "Bearer " + store.getters.accessToken;
}

// Init Framework7-Vue Plugin
Framework7.use(Framework7Vue);

Vue.filter('currencyFormat',(amount)=>{
  return "$ "+amount.toFixed(2);
});
// Init App
let vueInstance = new Vue({
  el: '#app',
  render: (h) => h(App),
  store,
  // Register App Component
  components: {
    app: App
  },
});
window.app = vueInstance;

window.addEventListener('offline',()=>{
  vueInstance.$f7.views.main.router.navigate('/no-internet/',{
    clearPreviousHistory: true,
    ignoreCache: true,
    history :false,
  });
}, false);