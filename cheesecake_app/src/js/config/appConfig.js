// const baseUrl = "https://rahmanasif.com/";
const baseUrl = "http://localhost:9003/";
// const baseUrl = "https://www.app-ccop.com/app_backend/";
const appConfig = {
  baseUrl: baseUrl,
  apiUrl: baseUrl + "api/v1/",
};

export default appConfig;