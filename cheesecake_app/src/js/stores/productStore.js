const storedProducts = localStorage.getItem('products');

const ProductStore = {
    state: {
        products: storedProducts ? JSON.parse(storedProducts) : null,
    },
    getters: {
        getAllProducts: (state) => {
            return state.products;
        },
        getProductByType: (state) => (type) => {
            if (type != null) {
                return state.products.filter(product => product.product_type === type);
            }
            return state.products;
        },
        getProductById: (state) => (id) => {
            return state.products.find(product => product.id === id);
        }
    },
    mutations: {
        FETCH_ALL_PRODUCTS: (state, products) => {
            localStorage.setItem('products', JSON.stringify(products));
            state.products = products;
        }
    },
    actions: {
        fetchAllProducts(context) {
            return app.axios
                .get('products')
                .then(response => {
                    console.log(response);
                    context.commit('FETCH_ALL_PRODUCTS', response.data.products);
                    return Promise.resolve('products_fetched');
                }).catch(error => {
                    return Promise.reject(error);
                    console.log(error);
                });
        }
    }

}

export default ProductStore;