const cartItems = localStorage.getItem('cart-items');
const CartStore = {
    state: {
        cartItems: cartItems ? JSON.parse(cartItems) : [],
        discount: 0,
        deliveryCharge: localStorage.getItem('delivery-charge') ?? 0,
    },
    getters: {
        getCartItems: (state) => {
            return state.cartItems;
        },
        getCartSubTotal: (state) => {
            return Object.values(state.cartItems).reduce((t, {
                price
            }) => Number.parseFloat(t) + Number.parseFloat(price), 0);
        },
        getCartDiscount: (state) => {
            return Number.parseFloat(state.discount);
        },
        getCartDeliveryCharge: (state) => {
            return Number.parseFloat(state.deliveryCharge);
        },
        getCartTotal: (state, getters) => {
            return getters.getCartSubTotal - getters.getCartDiscount + getters.getCartDeliveryCharge;
        }
    },
    mutations: {
        ADD_TO_CART: (state, cartItems) => {
            
            let alreadyInsertedProductIdIndex=state.cartItems.find(product=>product.productDetails.id==cartItems.productDetails.id);
            
            if(alreadyInsertedProductIdIndex && alreadyInsertedProductIdIndex.productDetails.product_type=='whole'){
                alreadyInsertedProductIdIndex.quantity++;
                alreadyInsertedProductIdIndex.selectedFlavors[0].flavor_qty=alreadyInsertedProductIdIndex.quantity;
                alreadyInsertedProductIdIndex.price=alreadyInsertedProductIdIndex.quantity*alreadyInsertedProductIdIndex.productDetails.product_price;
            }
            else{
                state.cartItems.push(cartItems);
            }


            localStorage.setItem('cart-items', JSON.stringify(state.cartItems));
        },
        UPDATE_CART: (state, cartItems) => {
            state.cartItems = cartItems;
            localStorage.setItem('cart-items', JSON.stringify(state.cartItems));
        },
        SET_DELIVERY_CHARGE: (state, deliveryCharge) => {
            state.deliveryCharge=deliveryCharge;
            localStorage.setItem('delivery-charge',deliveryCharge);
        },
        EMPTY_CART:(state)=>{
            state.cartItems=[];
            state.deliveryCharge=0;
            localStorage.removeItem('cart-items');
            localStorage.setItem('delivery-charge',0);
        }
    },
    actions: {

    }
}

export default CartStore;