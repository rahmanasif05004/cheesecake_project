import Vue from 'vue';
import Vuex from 'vuex';
import AuthStore from '../stores/authStore';
import ProductStore from '../stores/productStore';
import CartStore from "./cartStore";
import OrderStore from "./orderStore";
import app from 'framework7-vue/components/app';
Vue.use(Vuex);
export default new Vuex.Store({
    modules: {
        AuthStore,
        ProductStore,
        CartStore,
        OrderStore,
    },
});