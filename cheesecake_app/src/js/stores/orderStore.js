const storedOrders = localStorage.getItem('orders');
const OrderStore={
    state:{
        orders: storedOrders?JSON.parse(storedOrders):null,
    },
    getters:{
        getOrderDetails:(state)=>(orderId)=>{
            // console.log(state.orders);
            return state.orders.find(order=>order.id==orderId);
        }
    },
    mutations:{
        SET_ORDER:(state,orders)=>{
            localStorage.setItem('orders',JSON.stringify(orders));
            state.orders=orders;
        }
    },
    actions:{

    }
}

export default OrderStore;