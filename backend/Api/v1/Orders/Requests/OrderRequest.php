<?php

namespace Api\v1\Orders\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'delivery_address'=>Rule::requiredIf(function(){
                return request('delivery_charge')>0;
            }),
            'delivery_date'=>[Rule::requiredIf(function(){
                return request('delivery_charge')>0;
            }),'date','nullable'],
            'payment_method'=>'required',
            'delivery_charge'=>'numeric',
            'pickup_date'=>[Rule::requiredIf(function(){
                    return request('delivery_charge')===0;
            }),'date','nullable'],
            'cart_items'=>[function($attribute,$value,$fail){
                count($value)==0?$fail("Your cart is empty"):false;
            }]
        ];
    }

    /**
     * attributes can be changed here like the following
     * 'user_name'=>'User Name', or 'user_name'=>trnas('your_translation_file.user_name')
     * @return [type] [description]
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * form validation messages can be changed and translated by this method like the following
     * 'user_name.required'=>'Please provide user name' or 'user_name.required'=>trans('your_translation_file.user_name_msg')
     * @return [type] [description]
     */
    public function messages()
    {
        return [
            'delivery_address.required'=>'Please, let us know your delivery address',
            'delivery_date.required'=>'On which date, you want it to be delivered?',
            'payment_method.required'=>'Please, select a payment method',
            'delivery_charge.numeric'=>'Delivery charge needs to be in numeric format',
            'pickup_date.required'=>'On which date, you want to pick up your order?',
        ];
    }

}
