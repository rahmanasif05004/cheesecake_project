<?php

namespace Api\v1\Orders\Models;
use Api\v1\Products\Models\Product;
use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $table = 'order_details';
    protected $fillable = [
        'order_id',
        'product_id',
        'price',
        'quantity',
        'selected_flavors',
        'selected_packages'
    ];

    public function productName()
    {
        return $this->belongsTo(Product::class,'product_id','id')->select(['id','product_name','product_type']);
    }

}
