<?php

namespace Api\v1\Orders\Models;
use Illuminate\Database\Eloquent\Model;
use Api\v1\Orders\Models\OrderDetail;
use Illuminate\Http\Response as Res;
use Illuminate\Support\Carbon;
use Tymon\JWTAuth\Facades\JWTAuth;
use DB;
class Order extends Model
{
    protected $table = 'orders';
    protected $fillable = [
        'user_id',
        'pickup_date',
        'delivery_date',
        'delivery_address',
        'payment_method',
    ];

    protected $guarded=[
        'transaction_id',
        'transaction_response',
        'payment_status',
        'order_status',
        'delivery_charge',
        'discount_amount',
        'paid_amount',
        'order_amount',
    ];

    public function orderDetails()
    {
        return $this->hasMany(OrderDetail::class,'order_id','id');
    }

    public function setDeliveryDateAttribute($delivery_date)
    {
        $this->attributes['delivery_date']=Carbon::parse($delivery_date)->format("Y-m-d");
    }
    public function getDeliveryDateAttribute($delivery_date)
    {
        if($delivery_date!=null)
        {
            return Carbon::parse($delivery_date)->format("Y-m-d");
        }
    }

    public function setPickupDateAttribute($pickup_date)
    {
        $this->attributes['pickup_date']=Carbon::parse($pickup_date)->format("Y-m-d");
    }

    public function getPickupDateAttribute($pickup_date)
    {
        if($pickup_date!==null)
        {
            return Carbon::parse($pickup_date)->format("Y-m-d");
        }
    }

    public function scopeGenerateOrder($query,$orderRequest)
    {
        $checkLoggedInUser=JWTAuth::parseToken()->authenticate();
        if(!$checkLoggedInUser)
        {
            return [
                'message'=>'You are not authenticate to place order. Please, login again',
                'order'=>null,
                'status_code'=>Res::HTTP_UNAUTHORIZED,
            ];
        }
        $userId=$checkLoggedInUser->user_id;
        DB::beginTransaction();
        try{
            $order=$this->saveOrderBasicInfo($userId,$orderRequest);
            $this->saveOrderDetails($order,$orderRequest);
            DB::commit();
            return [
                'message'=>'Order placed successfully',
                'order'=>$order,
                'status_code'=>Res::HTTP_OK,
            ];
        }
        catch (\Exception $e){
            DB::rollback();

            return [
                'message'=>$e->getMessage(),
                'order'=>$order,
                'status_code'=>Res::HTTP_INTERNAL_SERVER_ERROR
            ];
        }
    }

    private function saveOrderBasicInfo($userId,$orderRequest)
    {
        $order=new Order();
        $order->user_id=$userId;
        $order->pickup_date=$orderRequest->pickup_date;
        $order->delivery_date=$orderRequest->delivery_date;
        $order->delivery_address=$orderRequest->delivery_address;
        $order->payment_method=$orderRequest->payment_method?'Paypal':null;
        $order->order_amount=collect($orderRequest->cart_items)->sum('price');
        $order->delivery_charge=$orderRequest->delivery_charge??0;
        $order->discount_amount=$orderRequest->discount_amount??0;
        $order->save();
        return $order;
    }

    private function saveOrderDetails($orderInstance,$orderRequest)
    {
        $orderDetails=[];
        foreach($orderRequest->cart_items as $key=>$value)
        {
            $orderDetails[$key]['product_id']=$value['productDetails']['id'];
            $orderDetails[$key]['price']=$value['price'];
            $orderDetails[$key]['quantity']=$value['quantity']??$value['selectedPackage'];
            $orderDetails[$key]['selected_flavors']=json_encode($value['selectedFlavors']??[]);
            $orderDetails[$key]['selected_packages']=$value['quantity']??$value['selectedPackage'];
        }

        $orderInstance->orderDetails()->createMany($orderDetails);
    }
}
