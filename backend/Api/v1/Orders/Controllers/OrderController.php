<?php

namespace Api\v1\Orders\Controllers;

use Api\v1\Orders\Models\OrderDetail;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;
use Illuminate\Http\Response as Res;
use Api\v1\Orders\Requests\OrderRequest;
use Api\v1\Orders\Models\Order;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Facades\JWTAuth;


class OrderController extends Controller
{
    public function index()
    {
        $loggedInUser=JWTAuth::user();
        $orders=Order::with('orderDetails.productName')->orderBy('id','desc');
        if($loggedInUser->user_type==='customer')
        {
            $orders->whereUserId($loggedInUser->user_id);
        }

        return Response::json($orders->get(),Res::HTTP_OK);
    }

    public function show($id)
    {
        $orders=Order::with('orderDetails.productName')->whereId($id)->orderBy('id','desc');  
        return Response::json($orders->first(),Res::HTTP_OK);
    }

    public function store(OrderRequest $request)
    {
        $order=Order::GenerateOrder($request);
        return Response::json($order,$order['status_code']);
    }

    public function paypalPaymentView($orderId)
    {
        $data['order']=Order::find($orderId);
        return view('paypal-payment-form',$data);
    }

    public function ipn(Request $request)
    {
        $orderId=$request->custom;
        $order=Order::find($orderId);
        $order->transaction_id=$request->txn_id;
        $order->transaction_response=json_encode($request->all());
        $order->payment_status=$request->payment_status;
        $order->order_status="Processing";
        $order->paid_amount=$request->payment_gross;
        $order->save();

        return Response::json(['message'=>'Order submitted'],Res::HTTP_OK);

    }

    /**
     * Need to remove and to change end point in mobile app api call
     * @return mixed
     */
    public function orderListByCustomer()
    {
        $loggedInUser=JWTAuth::parseToken()->authenticate();

        $orderByUser=Order::whereUserId($loggedInUser->user_id)->with('orderDetails.productName')->orderBy('id','desc')->get();
        return Response::json($orderByUser,Res::HTTP_OK);
    }

    public function update(Request $request,$id)
    {
        try
        {
            $loggedInUser=JWTAuth::user();
            if($loggedInUser->user_type==='admin')
            {
                $order=Order::findOrFail($id);
                if($request->update_type==='update_order_status')
                {
                    $order->order_status=$request->order_status;
                    $message="Order status changed successfully";
                }
                else {
                    $message="Order updated successfully";
                }
                $order->save();

            }
            return Response::json($message,Res::HTTP_OK);

        }
        catch (ModelNotFoundException $e)
        {
            return Response::json($e->getMessage(),Res::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    public function destroy($id)
    {
        try
        {
            $order=Order::findOrFail($id);
            if($order->order_status==='Cancelled') {
                $order->delete();
                return Response::json("Deleted Successfully",Res::HTTP_OK);
            }
            else{
                return Response::json("Before delete you have to cancel the order first",Res::HTTP_UNPROCESSABLE_ENTITY);
            }
        }
        catch (ModelNotFoundException $e)
        {
            return Response::json($e->getMessage(),Res::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function totalPendingOrders()
    {
        $totalPendingOrders=Order::whereOrderStatus('Processing')->count();
        return Response::json(['total_pending_orders'=>$totalPendingOrders],Res::HTTP_OK);
    }
}
