<?php

Route::prefix('api/v1/')
->namespace('Api\v1\Orders\Controllers')
->middleware(['jwt.verify'])
->group(function () {
    Route::get('orders/paypal-payment-form/{orderId}','OrderController@paypalPaymentView');
    Route::post('orders/ipn','OrderController@ipn')->name('orders.ipn');
    Route::get('orders/order-list-by-customers','OrderController@orderListByCustomer');
    Route::get('orders/total-pending-orders','OrderController@totalPendingOrders');
    Route::resource('orders','OrderController');
});
