<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->dateTime('pickup_date')->nullable();
            $table->dateTime('delivery_date')->nullable();
            $table->text('delivery_address')->nullable();
            $table->string('payment_method',20);
            $table->string('transaction_id')->nullable();
            $table->text('transaction_response')->nullable();
            $table->string('payment_status')->nullable();
            $table->string('order_status')->nullable();
            $table->float('order_amount',10,2)->default(0);
            $table->float('delivery_charge',10,2)->default(0);
            $table->float('discount_amount',10,2)->default(0);
            $table->float('paid_amount',10,2)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
