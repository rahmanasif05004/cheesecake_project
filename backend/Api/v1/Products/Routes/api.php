<?php

Route::prefix('api/v1/')
->namespace('Api\v1\Products\Controllers')
->middleware(['jwt.verify'])
->group(function () {
    Route::post("upload",'ProductController@upload');
    Route::resource('products','ProductController');
});
