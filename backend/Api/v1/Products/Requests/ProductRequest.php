<?php

namespace Api\v1\Products\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules=[
            'product_description'=>'max:1000',
            'product_type'=>'required',
            'product_price'=>'exclude_if:product_type,cup|numeric|gt:0',
            'selectedFlavorsIds'=>'required|array',
            'packages.*.package_name'=>'required_if:product_type,cup|distinct',
            'packages.*.package_price'=>'nullable|required_if:product_type,cup|numeric|gt:0',
        ];
        if(request()->isMethod('POST'))
        {
            $rules['product_name']='required|unique:products|max:250';
        }
        else
        {
            $rules['product_name']='required|max:250|unique:products,id,'.request('id');
        }
        return $rules;

    }

    /**
     * attributes can be changed here like the following
     * 'user_name'=>'User Name', or 'user_name'=>trnas('your_translation_file.user_name')
     * @return [type] [description]
     */
    public function attributes()
    {
        return [
            // 'product_type'=>'type'
        ];
    }

    /**
     * form validation messages can be changed and translated by this method like the following
     * 'user_name.required'=>'Please provide user name' or 'user_name.required'=>trans('your_translation_file.user_name_msg')
     * @return [type] [description]
     */
    public function messages()
    {
        return [
            'packages.*.package_name.required_if'=>'required',
            'packages.*.package_name.distinct'=>'duplicate',
            'packages.*.package_price.required_if'=>'required',
            'packages.*.package_price.numeric'=>'must be numeric',
            'packages.*.package_price.gt'=>'It is free !',
            'product_price.gt'=>'It is free !!!!!',
        ];
    }

}
