<?php

namespace Api\v1\Products\Controllers;

use Api\v1\Products\Models\ProductImage;
use Illuminate\Http\Request;
use Response;
use Illuminate\Http\Response as Res;
use Api\v1\Products\Requests\ProductRequest;
use Api\v1\Products\Models\Product;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Arr;
use Api\v1\Orders\Models\OrderDetail;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [];
        $data['products'] = Product::LoadByProductType($request->product_type??'all')->ActiveProductOnly()->with(['flavors','packages','productImages'])->get();
        return Response::json($data,Res::HTTP_OK);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [];
        return Response::json($data,Res::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        try{
            $generatedCake=Product::GenerateCake($request);
            $this->upload((object)$request->uploadedFiles,$generatedCake->id);
            return Response::json($generatedCake,Res::HTTP_CREATED);
        }
        catch(ModelNotFoundException $e)
        {
            return Response::json('Sorry, Operation Failed',Res::HTTP_NOT_FOUND);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try
        {
            $data['product'] = Product::findOrFail($id);
            return Response::json($data,Res::HTTP_OK);
        }
        catch(ModelNotFoundException $e)
        {
            return Response::json('Sorry the data is not found',Res::HTTP_NOT_FOUND);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try
        {
            $data['product'] = Product::whereId($id)->with(['flavors','packages','productImages'])->first();
            return Response::json($data,Res::HTTP_OK);
        }
        catch(ModelNotFoundException $e)
        {
            return Response::json('Sorry the data is not found',Res::HTTP_NOT_FOUND);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, $id)
    {
        try{
            $generatedCake=Product::UpdateCake($request);
//            $this->upload((object)$request->uploadedFiles,$generatedCake->id);
            return Response::json($generatedCake,Res::HTTP_CREATED);
        }
        catch(ModelNotFoundException $e)
        {
            return Response::json('Sorry, Operation Failed',Res::HTTP_NOT_FOUND);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try
        {
            $product = Product::findOrFail($id);
            $ifProductAlreadyOrdered=OrderDetail::whereProductId($id)->count();
            if($ifProductAlreadyOrdered)
            {
                $product->product_status='inactive';
                $product->save();
                return Response::json([
                    'message' => 'Product Deactivated because it is already in orders',
                    'status' => true
                ],Res::HTTP_OK);
            }
            
            if($product->delete()){
                return Response::json([
                    'message' => 'Succefully Deleted',
                    'status' => true
                ],Res::HTTP_OK);
            }else{
                return Response::json([
                    'message' => "Cannot be deleted",
                    'status' => false
                ],Res::HTTP_NOT_FOUND );
            }
        }
        catch(ModelNotFoundException $e)
        {
            return Response::json('Sorry, Operation Failed',Res::HTTP_NOT_FOUND);
        }
    }

    private function upload($filesToBeUploaded,$productId)
    {
        $files=Arr::collapse($filesToBeUploaded);
        $imagesData=[];
        foreach($files as $key=>$file)
        {
            $fileName=time().$key.'.'.$file->extension();
            $file->move(public_path('uploads'), $fileName);
            $imagesData[$key]['product_id']=$productId;
            $imagesData[$key]['image_name']=$fileName;
        }
        ProductImage::insert($imagesData);
    }
}
