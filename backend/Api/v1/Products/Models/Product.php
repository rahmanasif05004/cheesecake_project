<?php

namespace Api\v1\Products\Models;
use Api\v1\Flavors\Models\Flavor;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $fillable = [
        'product_name',
        'product_description',
        'product_price',
        'product_type',
        'product_status',
    ];
    protected $appends=['product_thumbimages'];


    public static function boot()
    {
        parent::boot();
        static::creating(function($model){
            //you can save any field value on inserting data into a table
            //for example $model->created_by=Auth::user()->id
        });

        static::updating(function($model){
            //you can save any field value on updating data into a table
            //for example $model->updated_by=Auth::user()->id
        });

    }

    public function getProductPriceAttribute($product_price)
    {
        if($this->product_type=='cup')
        {
            $product_price= min(array_column($this->packages->toArray(),'package_price'));
        }
        return number_format($product_price,2);
    }
    public function flavors()
    {
        return $this->belongsToMany(Flavor::class);
    }

    public function packages()
    {
        return $this->hasMany(ProductPackage::class,'product_id','id');
    }

    public function productImages()
    {
        return $this->hasMany(ProductImage::class,'product_id','id');
    }


    public function getProductThumbimagesAttribute()
    {
        if($this->relationLoaded('productImages'))
        {
            return count($this->relations['productImages'])
                ?$this->relations['productImages'][0]
                :['caption'=>'','url'=>''];
        }
    }
    public function scopeLoadByProductType($query,$type)
    {
        return $type!=="all"?$query->whereProductType($type):$query;
    }
    public function scopeActiveProductOnly($query)
    {
        return $query->whereProductStatus('active');
    }
    public function scopeGenerateCake($query,$request)
    {
        $product = new Product();
        $data = $request->only($product->getFillable());
        $product->fill($data)->save();
        $product->flavors()->attach($request->selectedFlavorsIds);
        if($request->product_type==='cup')
        {
            $product->packages()->createMany($this->buildCakePackages($request->packages));
        }
        return $product;
    }

    public function scopeUpdateCake($query,$request)
    {
        $product = Product::findOrFail($request->id);
        $data = $request->only($product->getFillable());
        $product->fill($data)->save();
        $product->flavors()->sync($request->selectedFlavorsIds);
        if($request->product_type==='cup')
        {
            $product->packages()->delete();
            $product->packages()->createMany($this->buildCakePackages($request->packages));
        }
        return $product;
    }
    private function buildCakePackages($cakePackages)
    {
        $packages=[];
        foreach($cakePackages as $key=>$package)
        {
            $packages[$key]['package_name']=$package['package_name'];
            $packages[$key]['package_price']=$package['package_price'];
        }
        return $packages;
    }
}
