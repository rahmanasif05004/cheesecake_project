<?php

namespace Api\v1\Products\Models;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    protected $fillable=['image_name','product_id'];
    protected $appends=['url','caption'];//for f7 photo browser that receives only url and caption as object keys

    public function getUrlAttribute()
    {
        return url('cheese_app/public/uploads/'.$this->image_name);
    }

    public function getCaptionAttribute()
    {
        return $this->image_name;
    }
}
