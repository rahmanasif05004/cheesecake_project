<?php


namespace Api\v1\Products\Models;
use Illuminate\Database\Eloquent\Model;

class ProductPackage extends Model
{
    protected $table = 'product_packages';
    protected $fillable = [
        'product_id',
        'package_name',
        'package_price',
    ];
}
