<?php

namespace Api\v1\Settings\Controllers;

use Illuminate\Http\Request;
use Response;
use Illuminate\Http\Response as Res;
use Api\v1\Settings\Requests\SettingRequest;
use Api\v1\Settings\Models\Setting;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
class SettingController extends Controller
{
   public function getBusinessSettings()
   {
        $businessSettings=Setting::all()->toArray();
        $fields=array_column($businessSettings,'field_name');
        $values=array_column($businessSettings,'field_value');
        return Response::json(array_combine($fields,$values),Res::HTTP_OK);

   }

   public function setBusinessSettings(Request $request)
   {
       Setting::truncate();
       $settings=[];
       foreach($request->all() as $key=>$value)
       {
           $settings[$key]['field_name']=$key;
           $settings[$key]['field_value']=$value;
       }
       Setting::insert($settings);
       return Response::json($settings,Res::HTTP_OK);
   }
}
