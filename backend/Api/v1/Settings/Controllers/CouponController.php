<?php

namespace Api\v1\Settings\Controllers;

use Illuminate\Http\Request;
use Response;
use Illuminate\Http\Response as Res;
use Api\v1\Settings\Requests\CouponRequest;
use App\Http\Controllers\Controller;
use Api\v1\Settings\Models\Coupon;

class CouponController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CouponRequest $request)
    {
        $coupon=new Coupon();
        $coupon->coupon_code=$request->coupon_code;
        $coupon->coupon_percentage=$request->coupon_percentage;
        $coupon->expiration_date=$request->expiration_date;
        $coupon->save();
        return Response::json(['message'=>'Coupon created successfully','coupon'=>$coupon],Res::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
}
