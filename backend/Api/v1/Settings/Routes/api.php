<?php

Route::prefix('api/v1/')
->namespace('Api\v1\Settings\Controllers')
//->middleware(['jwt.verify'])
->group(function () {
    Route::get('business-settings',[\Api\v1\Settings\Controllers\SettingController::class,'getBusinessSettings']);
    Route::post('set-business-settings',[\Api\v1\Settings\Controllers\SettingController::class,'setBusinessSettings']);
    Route::resource('coupons','CouponController');
});
