<?php

namespace Api\v1\Flavors\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FlavorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       
        request()->isMethod('POST')
            ?$rules['flavor_name']='required|unique:flavors'
            :$rules['flavor_name']='required|unique:flavors,flavor_name,'.$this->route('flavor');
        $rules['flavor_note']='max:500';
        $rules['flavor_stock_qty']='required|numeric|gt:0';
        return $rules;
    }

    /**
     * attributes can be changed here like the following
     * 'user_name'=>'User Name', or 'user_name'=>trnas('your_translation_file.user_name')
     * @return [type] [description]
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * form validation messages can be changed and translated by this method like the following
     * 'user_name.required'=>'Please provide user name' or 'user_name.required'=>trans('your_translation_file.user_name_msg')
     * @return [type] [description]
     */
    public function messages()
    {
        return [
            //
        ];
    }

}
