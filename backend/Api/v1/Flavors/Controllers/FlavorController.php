<?php

namespace Api\v1\Flavors\Controllers;

use Illuminate\Http\Request;
use Response;
use Illuminate\Http\Response as Res;
use Api\v1\Flavors\Requests\FlavorRequest;
use Api\v1\Flavors\Models\Flavor;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
class FlavorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
        $data['flavors'] = Flavor::all();
        return Response::json($data,Res::HTTP_OK);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [];
        return Response::json($data,Res::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FlavorRequest $request)
    {
        try{
            $flavor = new Flavor();
            $data = $request->only($flavor->getFillable());
            $flavor->fill($data)->save();
            return Response::json($data,Res::HTTP_CREATED);
        }
        catch(ModelNotFoundException $e)
        {
            return Response::json('Sorry, Operation Failed',Res::HTTP_NOT_FOUND);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try
        {
            $data['flavor'] = Flavor::findOrFail($id);
            return Response::json($data,Res::HTTP_OK);
        }
        catch(ModelNotFoundException $e)
        {
            return Response::json('Sorry the data is not found',Res::HTTP_NOT_FOUND);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try
        {
            $data['flavor'] = Flavor::findOrFail($id);
            return Response::json($data,Res::HTTP_OK);
        }
        catch(ModelNotFoundException $e)
        {
            return Response::json('Sorry the data is not found',Res::HTTP_NOT_FOUND);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FlavorRequest $request, $id)
    {
        try
        {
            $flavor = Flavor::findOrFail($id);
            $data = $request->only($flavor->getFillable());
            $flavor->update($data);
            return Response::json($data,Res::HTTP_CREATED);
        }
        catch(ModelNotFoundException $e)
        {
            return Response::json('Sorry, Operation Failed',Res::HTTP_NOT_FOUND);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try
        {
            $flavor = Flavor::findOrFail($id);
            if($flavor->delete()){
                return Response::json([
                    'message' => 'Succefully Deleted',
                    'status' => true
                ],Res::HTTP_OK);
            }else{
                return Response::json([
                    'message' => "Cannot be deleted",
                    'status' => false
                ],Res::HTTP_NOT_FOUND );
            }
        }
        catch(ModelNotFoundException $e)
        {
            return Response::json('Sorry, Operation Failed',Res::HTTP_NOT_FOUND);
        }
    }
}