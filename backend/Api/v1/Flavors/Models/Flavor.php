<?php

namespace Api\v1\Flavors\Models;
use Illuminate\Database\Eloquent\Model;

class Flavor extends Model
{
    protected $table = 'flavors';
    protected $fillable = [
        'flavor_name',
        'flavor_note',
        'flavor_stock_qty'
    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function($model){
            //you can save any field value on inserting data into a table
            //for example $model->created_by=Auth::user()->id
        });

        static::updating(function($model){
            //you can save any field value on updating data into a table
            //for example $model->updated_by=Auth::user()->id
        });
    }
}
