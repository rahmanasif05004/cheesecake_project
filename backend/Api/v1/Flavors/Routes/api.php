<?php

Route::prefix('api/v1/')
->namespace('Api\v1\Flavors\Controllers')
->middleware(['jwt.verify'])
->group(function () {
    Route::resource('flavors','FlavorController');
});
