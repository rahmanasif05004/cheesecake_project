<?php

Route::prefix('api/v1/')
->namespace('Api\v1\Users\Controllers')
->group(function () {

    //public routes
    Route::post('/login','AuthenticationController@login');
    Route::post('/registration','RegistrationController');

    //private routes
    Route::middleware(['jwt.verify'])->group(function (){
        Route::resource('users','UserController');
        Route::post('/logout','AuthenticationController@logout');
    });
});
