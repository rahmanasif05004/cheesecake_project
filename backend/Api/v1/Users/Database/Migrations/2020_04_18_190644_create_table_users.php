<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('user_id');
            $table->string('first_name',250);
            $table->string('last_name',250)->nullable();
            $table->string('email',250)->unique();
            $table->string('password',250);
            $table->string('city',250)->nullable();
            $table->string('address',250)->nullable();
            $table->string('phone',250)->nullable();
            $table->string('profile_image',250)->nullable();
            $table->enum('user_type',['customer','admin']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
