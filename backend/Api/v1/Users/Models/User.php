<?php

namespace Api\v1\Users\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    protected $table = 'users';
    protected $primaryKey="user_id";
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'phone',
        'address',
        'city',
        'profile_image',
        'user_type',
        'password',
    ];
    protected $hidden=['password'];
    protected $appends=['full_name'];

    public static function boot()
    {
        parent::boot();
        // static::creating(function($model){
        //     $model->password=bcrypt($model->password);
        //     //you can save any field value on inserting data into a table
        //     //for example $model->created_by=Auth::user()->id
        // });

        static::saving(function($model){
            bcrypt($model->password);
            if(request()->password!=$model->getOriginal('password') || request()->password!=='')
            {
                $model->password=bcrypt($model->password);
            }
            else
            {
                unset($model->password);
            }
            //you can save any field value on updating data into a table
            //for example $model->updated_by=Auth::user()->id
        });
    }

//    public function setPasswordAttributes($password)
//    {
//
//    }
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function getFullNameAttribute()
    {
        return $this->first_name.' '.$this->last_name;
    }
    public function getPhoneAttribute($phone)
    {
        return $phone??'N/A';
    }
}
