<?php


namespace Api\v1\Users\Controllers;
use Response;
use Api\v1\Users\Requests\UserRequest;
use Api\v1\Users\Models\User;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Facades\JWTAuth;

class RegistrationController extends Controller
{
    public function __invoke(UserRequest $request)
    {
        try
        {
            $user= new User();
            $data=$request->only($user->getFillable());
            $user->fill($data)->save();
            $token=JWTAuth::fromUser($user);
            return $this->respondWithToken($token,$user,'test');
        }
        catch(\Exception $e)
        {
            return $this->respondInternalError($e->getMessage());
        }
    }
}
