<?php
namespace Api\v1\Users\Controllers;

use Illuminate\Http\Request;
use Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AuthenticationController extends Controller
{
    public function login(Request $request)
    {
        $request->validate([
            'email'     =>'required',
            'user_type' =>'required',
            'password'  =>'required'
        ]);

        $token=$this->guard()->attempt([
            'password'  =>$request->password,
            'email'     =>$request->email,
            'user_type' =>$request->user_type,
        ]);

        if($token)
        {
            return $this->respondWithToken(
                $token,
                $this->guard()->user(),
                $this->guard()->factory()->getTTL() * 6000
            );
        }
        else
        {
            return $this->respondWithError("Wrong Credentials");
        }
    }

    public function logout()
    {
        $this->guard()->logout();
    }

    private function guard($guard='api')
    {
        return Auth::guard($guard);
    }
}
