const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   .scripts(
      [
         'resources/js/plugins/plugins/jquery/jquery.min.js',
         'resources/js/plugins/plugins/bootstrap/js/bootstrap.bundle.min.js',
          'resources/js/plugins/plugins/jquery-easing/jquery.easing.min.js',
          'resources/js/plugins/admin.min.js',
      ],
      'public/js/all.js'
   )
   .sass('resources/sass/app.scss', 'public/css')
   .styles(
      [
         'resources/css/admin.css',
         'resources/css/style.css',
      ],
      'public/css/all.css'
   ).copyDirectory('resources/images', 'public/images');
