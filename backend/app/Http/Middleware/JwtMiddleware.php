<?php

namespace App\Http\Middleware;

use App\Traits\ResponseJsonAble;
use Closure;
use Exception;
use Illuminate\Session\TokenMismatchException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;
use Tymon\JWTAuth\Facades\JWTAuth;

class JwtMiddleware extends BaseMiddleware
{
    use ResponseJsonAble;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try
        {
            $user=JWTAuth::parseToken()->authenticate();
        }
        catch(TokenInvalidException $e)
        {
            return $this->respondWithError("Invalid Token");
        }
        catch(TokenExpiredException $e)
        {
            return $this->respondWithError("Token Expired");
        }
        catch(TokenMismatchException $e)
        {
            return $this->respondWithError("Token Mismatched");
        }
        catch(Exception $e)
        {
            return $this->respondWithError($e->getMessage());
        }
        return $next($request);
    }
}
