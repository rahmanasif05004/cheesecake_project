export default{
    confirmDelete:(func)=>{
        vue.$swal.fire({
            title: 'Are you sure to delete?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
            if (result.value) {
                return func();
            }
        });
    },
    showAlert:(title,text,type)=>{
        vue.$swal.fire({
            title: title,
            text: text,
            icon: type,
        });
    }
}
