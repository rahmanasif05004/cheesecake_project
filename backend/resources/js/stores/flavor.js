import FlavorService from '../services/FlavorService';
const flavor={
    state:{
        flavors:[],
        flavorValidationErrors:[],
        flavorAddComponentKey:0,
        flashMessage:false,
        showFlavorEditForm:false,
        editableFlavor:[],
    },
    getters:{
        flavors(state){
            return state.flavors;
        },
        flavorValidationErrors(state){
            return state.flavorValidationErrors;
        },
        flavorAddComponentKey(state){
            return state.flavorAddComponentKey;
        },
        flashMessage(state){
            return state.flashMessage;
        },
        showFlavorEditForm(state){
            return state.showFlavorEditForm;
        },
        getEditableFlavor(state){
            return state.editableFlavor;
        }

    },
    mutations:{
        FLAVOR_CREATED:(state,payload)=>{
            state.flavorAddComponentKey++;
            state.flavorValidationErrors=[];
        },
        FLAVOR_VALIDATION_ERROR:(state,validationErrors)=>{
            state.flavorValidationErrors=validationErrors;
        },
        GET_FLAVORS:(state,flavors)=>{
            state.flavors=flavors;
        },
        FLAVOR_DELETED:(state)=>{

        },
        SEND_FLASH_MESSAGE:(state,message)=>{
            state.flashMessage=message;
            setTimeout(()=>{
                state.flashMessage=false;
            },3000);
        },
        FLAVOR_EDIT_REQUEST:(state,editableFlavor)=>{
            state.showFlavorEditForm=true;
            state.editableFlavor=editableFlavor;
            // state.flavors.find(flavor=>flavor.id==flavorId);
        },
        FLAVOR_UPDATED:(state,payload)=>{
            state.showFlavorEditForm=false;
            state.flavorValidationErrors=[];
        },

    },
    actions:{
        createFlavor(context,flavor){
            return axios
                .post('/api/v1/flavors',flavor)
                .then(response=>{
                    context.commit('FLAVOR_CREATED');
                    context.dispatch('getFlavors');
                })
                .catch(error=>{
                    if(error.response.status===422){
                        context.commit('FLAVOR_VALIDATION_ERROR',error.response.data.errors);
                    }
                    else{
                        console.log(error.response);
                    }
                });
        },
        editFlavor(context,flavorId){
            context.commit('FLAVOR_EDIT_REQUEST',context.getters.flavors.find(flavor=>flavor.id==flavorId));
        },
        updateFlavor(context,flavor){
            return axios
                .put('/api/v1/flavors/'+flavor.id,flavor)
                .then(response=>{
                    context.commit('FLAVOR_UPDATED');
                    context.commit('SEND_FLASH_MESSAGE',"Updated Successfully");
                    context.dispatch('getFlavors');
                })
                .catch(error=>{
                    if(error.response.status===422){
                        context.commit('FLAVOR_VALIDATION_ERROR',error.response.data.errors);
                    }
                    else{
                        console.log(error.response);
                    }
                });
        },
        getFlavors({commit}){
            return axios
                .get('/api/v1/flavors')
                .then(response=>{
                    commit('GET_FLAVORS',response.data.flavors.reverse());
                })
                .catch(error=>{
                    console.log(error);
                });
        },
        deleteFlavor({dispatch,commit},flavorId){
            return axios
                .delete('/api/v1/flavors/'+flavorId)
                .then(response=>{
                    dispatch('getFlavors');
                    commit('SEND_FLASH_MESSAGE',"Deleted Successfully "+flavorId);
                })
                .catch(error=>{
                    console.log(error);
                });
        }
    }
}
export default flavor;