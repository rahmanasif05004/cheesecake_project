

const base={
    state:{
        showLoader:false,
        totalPendingOrders:0,
    },
    getters:{
        showLoader(state){
            return state.showLoader;
        },
        showTotalPeningOders(state){
            return state.totalPendingOrders;
        }
    },
    mutations:{
        SHOW_LOADER:(state,payload)=>{
            state.showLoader=true;
        },
        HIDE_LOADER:(state,payload)=>{
            state.showLoader=false;
        },
        UPDATE_TOTAL_PENDING_ORDERS:(state,totalPendingOrders)=>{
            state.totalPendingOrders=totalPendingOrders;
        }
    },
    actions:{
        getTotalPendingOrders(context){
            axios
            .get("/api/v1/orders/total-pending-orders")
            .then(response=>{
                context.commit('UPDATE_TOTAL_PENDING_ORDERS',response.data.total_pending_orders);
            })
            .catch(error=>{
                console.log(error);
            });
        }
    }
}
export default base;
