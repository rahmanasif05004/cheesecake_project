import Vue from 'vue';
import Vuex from 'vuex';
import auth from './auth';
import flavor from './flavor';
import base from "./base";

Vue.use(Vuex);
export default new Vuex.Store({
    modules:{
        auth,
        flavor,
        base,
        //other modules will be laoded here as per requirements
    }
});
