const auth={
    state:{
        authenticatedUser:(typeof localStorage.getItem('authenticated-user')!=='undefined')
            ?JSON.parse(localStorage.getItem('authenticated-user'))
            :null,
        isAuthenticated:localStorage.getItem('is-authenticated')?true:false,
    },
    getters:{
        getAuthenticatedUser(state){
            return state.authenticatedUser;
        },
        getIsAuthenticated(state){
            return state.isAuthenticated
        }
    },
    mutations:{
        LOGIN_SUCCESS:(state,authenticatedUserData)=>{
            state.isAuthenticated=true;
            state.authenticatedUser=authenticatedUserData;
            localStorage.setItem('authenticated-user',JSON.stringify(authenticatedUserData));
            localStorage.setItem('is-authenticated',true);
            axios.defaults.headers.common["Authorization"] = 'Bearer '+authenticatedUserData.access_token;
        },
        LOGOUT_SUCCESS:(state)=>{
            state.authenticatedUser=null;
            state.isAuthenticated=false;
            localStorage.removeItem('authenticated-user');
            localStorage.removeItem('is-authenticated');
        }
    },
    actions:{

    }
}

export default auth;
