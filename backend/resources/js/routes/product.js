import Flavor from '../components/pages/products/flavors/Flavor.vue';
import CreateProduct from "../components/pages/products/CreateProduct";
import ProductList from "../components/pages/products/ProductList";
import LayoutTemplate from "../components/layouts/LayoutTemplate";
import multiguard from 'vue-router-multiguard';
import IsAuthenticated from '../middlewares/IsAuthenticated';
import EditProduct from "../components/pages/products/EditProduct";
const ProductRoutes = [
    {
        path: '/flavors',
        component: LayoutTemplate,
        children: [
            {
                path:'/',
                name:'flavor',
                component: Flavor,
                beforeEnter:multiguard([IsAuthenticated]),
            }
        ]
    },
    {
        path:'/products',
        component: LayoutTemplate,
        children:[
            {
                path:'/',
                name:'product-list',
                component:ProductList,
                beforeEnter:multiguard([IsAuthenticated]),
            },
            {
                path:'create',
                name:'product-create',
                component:CreateProduct,
                beforeEnter:multiguard([IsAuthenticated]),
            },
            {
                path:'edit/:productId',
                name:'product-edit',
                component:EditProduct,
                beforeEnter:multiguard([IsAuthenticated]),
            }
        ]
    }
];

export default ProductRoutes;
