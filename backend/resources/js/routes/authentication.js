import AdminLogin from "../components/pages/authentications/AdminLogin";
import AdminRegistration from "../components/pages/authentications/AdminRegistration";
import multiguard from 'vue-router-multiguard';
import IfNotAuthenticated from "../middlewares/IfNotAuthenticated";
const AuthenticationRoutes=[
    {
        path:'/login',
        name:'login',
        component:AdminLogin,
        beforeEnter:multiguard([IfNotAuthenticated])
    },
    {
        path:'/register',
        name: 'register',
        component: AdminRegistration,
        beforeEnter:multiguard([IfNotAuthenticated])
    }

];

export default AuthenticationRoutes;
