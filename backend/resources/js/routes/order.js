import OrderList from "../components/pages/orders/OrderList";
import LayoutTemplate from "../components/layouts/LayoutTemplate";
import multiguard from "vue-router-multiguard";
import IsAuthenticated from "../middlewares/IsAuthenticated";
import OrderDetails from '../components/pages/orders/OrderDetails'
const OrderRoutes=[
    {
        path:'/orders',
        component:LayoutTemplate,
        children:[
            {
                path:'/',
                name:'order-lists',
                component:OrderList,
                beforeEnter:multiguard([IsAuthenticated]),
            },
            {
                path:'details/:orderId',
                name:'order-details',
                component:OrderDetails,
                beforeEnter:multiguard([IsAuthenticated]),
            },
        ]
    },
];

export default OrderRoutes;
