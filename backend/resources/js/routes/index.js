// import ExampleComponent from '../components/ExampleComponent';
// import Flavor from '../components/pages/products/flavors/Flavor.vue';
import ProductRoutes from '../routes/product';
import AuthenticationRoutes from "./authentication";
import SettingsRoutes from "./settings";
import OrderRoutes from "./order";
import CommonRoutes from "./common"; 

const routes = [
    ...ProductRoutes,
    ...AuthenticationRoutes,
    ...SettingsRoutes,
    ...OrderRoutes,
    ...CommonRoutes,
]

export default routes;
