import UserList from "../components/pages/users/UserList";
import LayoutTemplate from "../components/layouts/LayoutTemplate";
import multiguard from "vue-router-multiguard";
import IsAuthenticated from "../middlewares/IsAuthenticated";

const CommonRoutes=[
    {
        path:'/',
        component:LayoutTemplate,
        children:[
            {
                path:'/',
                name:'user-lists',
                component:UserList,
                beforeEnter:multiguard([IsAuthenticated]),
            },
        ]
    },
];

export default CommonRoutes;
