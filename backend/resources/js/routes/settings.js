import LayoutTemplate from "../components/layouts/LayoutTemplate";
import multiguard from "vue-router-multiguard";
import IsAuthenticated from "../middlewares/IsAuthenticated";
import BusinessSettings from "../components/pages/settings/BusinessSettings";
import CouponCreate from '../components/pages/settings/coupons/CouponCreate';
const SettingsRoutes=[
    {
        path:'/settings',
        component:LayoutTemplate,
        children:[
            {
                path:'/',
                name:'business-settings',
                component:BusinessSettings,
                beforeEnter:multiguard([IsAuthenticated]),
            }
        ]
    },
    {
        path:'/coupons',
        component:LayoutTemplate,
        children:[
            {
                path:'/',
                name:'coupon-create',
                component:CouponCreate,
                beforeEnter:multiguard([IsAuthenticated]),
            }
        ]
    }
];

export default SettingsRoutes;
