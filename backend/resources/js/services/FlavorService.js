import Axios from "axios";

class FlavorService{
    createFlavor(flavor){
        return axios.post('/api/v1/flavors',flavor);
    }

    getFlavors(){

    }

    editFlavor(){

    }

    updateFlavor(){

    }

    deleteFlavor(){

    }

    
}

export default new FlavorService;