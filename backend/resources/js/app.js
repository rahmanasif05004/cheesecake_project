require('./bootstrap');

import Vue from 'vue';
import App from './App.vue';
import VueRouter from 'vue-router';
import axios from 'axios';
import VueAxios from 'vue-axios';
// import 'bootstrap/dist/css/bootstrap.css';
// import 'bootstrap/dist/js/bootstrap.js';
// import '@fortawesome/fontawesome-free/css/all.css';
// import '@fortawesome/fontawesome-free/js/all.js';

import VModal from 'vue-js-modal';
Vue.use(VModal, { dynamicDefault: { draggable: true, resizable: true } })

import store from './stores';
import VueSweetalert2 from 'vue-sweetalert2';
import helpers from './helper';

import routes from './routes';
Vue.use(VueRouter);
Vue.use(VueAxios, axios);
Vue.use(VueSweetalert2);
import Toasted from 'vue-toasted';

Vue.use(Toasted,{
    singleton:true,
    duration:2000,
})


// axios.defaults.baseURL = window.location.origin+'/app_backend';
axios.defaults.baseURL = window.location.origin;
if (store?.getters?.getAuthenticatedUser?.access_token) {
    axios.defaults.headers.common["Authorization"] = 'Bearer ' + store.getters.getAuthenticatedUser.access_token;
}

//globalizing store
window.store = store;
//axios interceptors
axios.interceptors.response.use(response => {
    store.commit('HIDE_LOADER');
    return Promise.resolve(response);
}, error => {
    store.commit('HIDE_LOADER');
    if (error.response.status == 401) {
        store.commit('LOGOUT_SUCCESS');
        router.push('/login');
    }
    return Promise.reject(error);
});

axios.interceptors.request.use(config => {
    store.commit('SHOW_LOADER');
    return Promise.resolve(config);
}, error => {
    return Promise.reject(error);
});
const helper = {
    install(Vue, options) {
        Vue.prototype.$helpers = helpers; // we use $ because it's the Vue convention
    }
};
Vue.use(helper);


const router = new VueRouter({
    base: '/app_backend',
    mode: 'history',
    routes: routes
});




const app = new Vue(Vue.util.extend({
    router,
    store,
}, App)).$mount('#app');
window.vue = app;
